/* link morphology  */
/**
 * @author Jason Campbell
 * @email  jason@jasonscampbell.com
**/
(function(jQuery){
   jQuery.fn.extend({

	linkMorph: function( options ) {

		var defaults = {
		   spread    		:   { start : null, end : null },
		   grow				:   { start : null, end : null },
		   raise			:   { start : null, end : null },
		   lower			:   { start : null, end : null },
		   fade				:   { start : null, end : null },
		   color			:   { start : null, end : null },
		   shadow			:   { start : null, end : null },
		   spreadWave		:   { start : null, end : null },
		   growWave			:   { start : null, end : null },
			shrinkWave		:   { start : null, end : null },
		   fadeWave			:   { start : null, end : null },
		   colorWave		:   { start : null, end : null },
		   roller			:   { tapeLength : 2, start : null, end : null},
		   morphSpeed		:   2000,
		   mouseEvents		:   {on : 'mouseenter', off : 'mouseleave'},
		   tween			:   'easeOutBounce'
		};

		$.animating=false;

		var animateProperties={
			spread		:		'letterSpacing',
			grow		:		'fontSize',
			raise		:		'paddingBottom',
			lower		:		'marginTop',
			fade		:		'opacity',
			color		:		'color',
			shadow		:		'textShadow',
			roller		: 		'marginTop'
		}

		var exceptions=['roller'];	// link morph kinds that require special handling

		//set chosen morph types as element data before extending defaults
		$this=this;
		var morphOptions=[];
		jQuery.each(options, function(key, val){

			if(animateProperties[key.split("W")[0]])
				morphOptions.push(key);

		});

		$this.data({
			styles	:	morphOptions
		});

		var options =  jQuery.extend( defaults, options );

		return this.each( function(index) {
		   var $this=$(this);

			var propTargs=[];// css properties to animate

			for(i in morphOptions){

				var morphClass;

				var kind=animateProperties[morphOptions[i].split("W")[0]]; // 'letterSpacing'

				propTargs.push(kind);


					// set inital css (start) values
				var initCss={};
				initCss[kind]=options[morphOptions[i]].start;
				initCss['cursor']='pointer';

				if(typeof options[morphOptions[i]].start==='object')// if color morph set css start colors
					initCss['color']='RGB('+options[morphOptions[i]].start+')';

				$this.css(initCss);


				// set the name of the function to be called
				morphOptions[i].indexOf('Wave')>-1?
					morphClass='waveMorph'
				:
					morphClass='simpleMorph';

			}

			jQuery.each(exceptions, function(i){

				jQuery.each(morphOptions, function(y){

					if(exceptions[i]===morphOptions[y]){

					 $this[exceptions[index]](options, propTargs);

					}

				});

			});


			$this.mouseEvent(options, propTargs, morphClass);
		});


	},
	mouseEvent : function(options, propTargs, morphClass){
		var $this=$(this);

				// enable the use of single event listener type
			if(options.mouseEvents.on===options.mouseEvents.off){

				var eventOn=false;

				$this[options.mouseEvents.on](function(){
					if(eventOn===false){
						mouseEventOn();
						eventOn=true;
					}
					else{
						mouseEventOff();
						eventOn=false;
					}
				});

			}
			else{
				$this[options.mouseEvents.on](function(){
					mouseEventOn();
				});
				$this[options.mouseEvents.off](function(){
					mouseEventOff();
				});
			}

			function mouseEventOn(){

				if($.animating===true){
					$this.children().each(
					    function(){$(this).dequeue();
					});
					$.animating=false;
	//				$this.dequeue();

			//		$this.unwrapCharacters();
				}
				if($.animating===false){
					$this[morphClass](options,propTargs,1);
				}
				// if($this.children('span').length)
				// 	$this.unwrapCharacters();


			}

			function mouseEventOff(){

//				if($.animating===true){
					 $this.children().each(function(){$(this).dequeue();});
					 $.animating=false;
//				}
				$this[morphClass](options,propTargs,0);

			}


	},
	simpleMorph : function(options, propTargs, eventStage){

		var $this=this;

		var $morphs=$this.data('styles');
		var targetValue, properties={};

		 // set animate parameter values according to event stage and fill animate properties object
		for(i in propTargs){
			eventStage===1?
				targetValue=options[$morphs[i]].end
			:
				targetValue=options[$morphs[i]].start;

			properties[propTargs[i]]=targetValue;  // yields like: {property:value} (e.g., {letterSpacing:10})
		 }

			$this.dequeue()
			.animate(
			properties,
			{
				duration	:	options.morphSpeed,
				easing		:	options.tween,
				step		:	function(c,f){
					$.animating=true;
						return onStep(c);
				},
				complete	:	function(){

					$.animating=false;

					return onComplete();

				}
			}
		);

    },
    waveMorph : function(options, propTargs, eventStage){// put ea. char. in <span> and animate

		var $this=this;

// if(jQuery.inArray('color', propTargs)>-1){
// 	propTargs.splice(propTargs.indexOf('color'),1);
// 	$this.colorMorph(options,eventStage);
// }

		var $morphs=$this.data('styles');
		var targetValue, properties={}, delay=0;


		 // set animate parameter values according to event stage and fill animate properties object
		for(i in propTargs){

			eventStage===1?
				targetValue=options[$morphs[i]].end
			:
				targetValue=options[$morphs[i]].start;

			properties[propTargs[i]]=targetValue;  // start or end property and value
		}


		if(eventStage===1)//wrap ea. character in span
			$this.html($this.wrapCharacters());

		$this.children().each(function(){

			delay+=options.morphSpeed/($this.children('span').length-1);
			jQuery(this).data('delay',delay);
			$(this).animateEach(options, properties, eventStage);

		});

	},
	colorMorph : function(options, eventStage){

	},
	animateEach : function(options, properties, eventStage){

		var $this=this;

		delete properties.color;

			$this.animate(
			properties,
			{
				duration	:	$this.data('delay'),
				easing		:	options.tween,
				step		:	function(currentVal, effect){

					$.animating=true;

				 },
				 complete	:	function(){

					$.animating=false;

// $(this).dequeue();
					if(eventStage===0 && $this.attr('id')==$this.parent().data('length')-1){
						$this.parent().unwrapCharacters();
					}
				 }
			}

		);

	},
	wrapCharacters : function(){

	    var $this=this;

	    var linkText=$this.text();
	    var linkTextArray=linkText.split("");
	    var parsedLink='';

	    $this.data('length', linkTextArray.length);


		for(i in linkTextArray){
			if(linkTextArray[i]==" ")
				linkTextArray[i]='&nbsp;';

			parsedLink+='<span id='+i+' style="display:inline-block;">'+linkTextArray[i]+'</span>';
		}

	    return parsedLink;

	},
	unwrapCharacters : function(){

			var $this=this;
			var originString='';

			$this.children().each(function(index){
				originString+=$(this).text();
			});

		$this.html(originString);

	},
	roller : function(options, propTargs){

		var $this=$(this);

		$this.css({
			display:'inline-block'
		});

		var data=$this.data();


		var appendage='';

		var elWidth=$this.width(),
			elHeight=$this.height();

		var elText=$this.text();
		elWidth+=elText.split(' ').length-1;

		var tapeLength=options.roller.tapeLength,
			startColor=options.roller.startColor,
			endColor=options.roller.endColor;

		var rollLength=(elHeight+2)*(tapeLength-1)-2;


		var elPath	=	$this.attr('href')?$this.attr('href')
						:
							$this.attr('data-href')?$this.attr('data-href')
						:
							null,
			elTarg	=	$this.attr('target')?$this.attr('target')
						:
							$this.attr('data-target')?$this.attr('data-target')
						:
							'_self';


		var className='roller '+$('.roller').length;
		var classSyntax='.roller.'+$('.roller').length;

		var rollerObj=		'<div class="'+className+'">'
					+		'<img class="roller-back" src="images/rollup-bac.png" />'
					+		'<ul>'
					+			'<li>'
					+				$this.context.outerHTML
					+			'</li>'
					+		'</ul>'
					+		'<div class="window">'
					+		'<img class="roller-window t" src="images/rollup-top.png" />'
					+		'<img class="roller-window m" src="images/rollup-mid.png" />'
					+		'<img class="roller-window b" src="images/rollup-bot.png" />'
					+		'</div>'
					+	'</div>';

		 $this.replaceWith(rollerObj);

		var instance=$(classSyntax),
			roller=instance.find('ul'),
			shadow=instance.children('div');

		options.roller.start=parseInt(roller.css('marginTop'));
		options.roller.end=-rollLength;

		roller.data('styles', data.styles);

		for(var i=0;i<tapeLength;i++){
			appendage+='<li>' + $this.context.outerHTML + '</li>';
		}

		instance.height(elHeight+8)
					.width(elWidth+8);
		instance.find('img.m').width(elWidth);
//		shadow.height(instance.height()/3)
//						.width(elWidth+8);

		$(shadow.children().eq(0)).css({backgroundPosition:''});

		roller.width(elWidth);

		if(elPath){
			$this.removeAttr('href');
			instance.click(function(){
				window.open(elPath, elTarg)
			});
		}


		instance[options.mouseEvents.on](function(){

			roller.html(appendage);
			onStep=function(val){
				currentTop=Math.ceil(val);
	//			if(currentTop%elHeight==0)

			}

			onComplete=function(){

			}

			roller.simpleMorph(options,propTargs,1);

		});
		instance[options.mouseEvents.off](function(){

			onStep=function(val){
				currentTop=Math.floor(val);
			}

			onComplete=function(){
				if(parseInt(roller.css('marginTop'))===options.roller.start)
					roller.delay(2000).html('<li>' + $this.context.outerHTML + '</li>');
			}

			roller.simpleMorph(options,propTargs,0);

		});

	}


   });

})(jQuery);


jQuery(document).ready(function(){

	jQuery('h3').linkMorph({
		spreadWave : { start : 0 , end : 10 },
		growWave : { start : 20 , end : 60 },

		//spread : { start : -2 , end : 20 },
		//grow : { start : 20 , end : 60 },
		// lowerWave : { start : 0 , end : 10 },
		//fade : { start : .3 , end : 1 },
		tween : 'easeInOutBack',
		morphSpeed:1500
	});

	jQuery('a.one').linkMorph({
		spreadWave : { start : 0 , end : 10 },
		mouseEvents		:   {on : 'mouseenter', off : 'mouseenter'}
	});

	jQuery('a.two').linkMorph({
		growWave : { start : 20 , end : 30 },
	//	shadow : {start : 0, end : 3},
	//	colorWave : {start : [0,0,0], end : [255,255,255]},
		mouseEvents		:   {on : 'click', off : 'click'}
	});

	jQuery('h2.three').linkMorph({
		roller : {tapeLength : 3},
		tween : 'easeOutBounce',
		morphSpeed : 2000
	});
	jQuery('h1.three').linkMorph({
		roller : {tapeLength : 2},
		tween : 'easeOutBounce',
		morphSpeed : 2000
	});
	jQuery('a.three').linkMorph({
		roller : {tapeLength : 14},
		tween : 'easeOutBounce',
		morphSpeed : 2000
	});

});